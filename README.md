# Win Platform
![Win Platform](./screenshot.jpg) 

## Description
Win Platform is a module for Gallery 3 that provides a modified version of the admin dashboard's "Platform information" panel. It has been modified for use with "unsupported" server configurations. Specifically, it will display "Unknown" for Server load if server load is unavailable (rather then crashing like the default version does under windows) and displays "Server software" instead of Apache if the apache version is unavailable (instead of displaying "Unknown" for non-apache web servers like it does now).

This version has been tested with Gallery 3.0.2 using Windows / IIS. In theory the code should be flexible enough to run under any OS/Web server combination that you can get Gallery 3 running on, although no other configurations have been tested at this time.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "win_platform" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  Once activated you will be able to add the module to the dashboard from the dashboard content box.

## History
**Version 1.0.0:**
> - Initial Release.
> - Released on 04 July 2011.
>
> Download: [Version 1.0.0](/uploads/4d600210ca989c435446c48c17675740/win_platform100.zip)
